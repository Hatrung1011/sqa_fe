import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  formBCCN: FormGroup = this.fromBuilder.group({
    username: [null, []],
    password: [null, []],
    
  });
  constructor(private http: HttpClient,private fromBuilder: FormBuilder,){
      
  }
  onLogin(){
    let  loginForm = {
      username: this.formBCCN.controls['username'].value,
      password: this.formBCCN.controls['password'].value,

    };
    this.http
      .post('http://localhost:8080/auth/signup', loginForm)
      .subscribe((obj) => {
        console.log("sss",obj);
      });
  }
}

