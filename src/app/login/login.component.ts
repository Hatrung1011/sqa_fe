import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  formBCCN: FormGroup = this.fromBuilder.group({
    username: [null, []],
    password: [null, []],
 
});
  constructor(private http: HttpClient,private fromBuilder: FormBuilder, ) {

  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
  }

  onLogin() {
    debugger
   let  loginForm = {
      username: this.formBCCN.controls['username'].value,
      password: this.formBCCN.controls['password'].value,

    };
    this.http
      .post('http://localhost:8080/auth/login', loginForm)
      .subscribe((obj) => {
        console.log("sss",obj);
      });
  }
}
